# Скрипт базовой настройки рабочего места Ubuntu

Запускаем команды в терминале (Проверялось на Ubuntu 22.04)

```bash
curl "https://gitlab.com/api/v4/projects/44527766/repository/files/install.sh/raw" > install.sh
chmod 755 install.sh
sudo ./install.sh
```

### Установятся следующие приложения

- Opera
- Яндекс браузер
- Яндекс диск
- Криптопро
- Telegram
- Sublime Text 3 - текстовый редактор
- Viber (возможны проблемы с установкой, смотреть по ситуации)