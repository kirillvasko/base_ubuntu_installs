#!/bin/bash

#install basics
apt-get update
apt-get install -y wget snapd vim flameshot fuse binutils

#install opera
wget -qO- https://deb.opera.com/archive.key | apt-key add -
add-apt-repository -y "deb [arch=i386,amd64] https://deb.opera.com/opera-stable/ stable non-free"
apt install -y opera-stable

#install fish
apt-add-repository -y ppa:fish-shell/release-3
apt-get update
apt-get install -y fish
chsh -s $(which fish)

#install sublime
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/sublimehq-archive.gpg > /dev/null
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt-get update
sudo apt-get install sublime-text

# download packages
mkdir tmp
cd tmp
wget http://192.168.1.235:5665/linux-amd64_deb.tgz
wget http://192.168.1.235:5665/Yandex.deb
wget http://192.168.1.235:5665/yandex-disk_latest_amd64.deb

# install crypropro

tar -xf linux-amd64_deb.tgz
cd linux-amd64_deb
./install_gui.sh
cd ../

#install yandex browser
apt-get install -y ./Yandex.deb

#install and configure yandex disk
apt-get install -y ./yandex-disk_latest_amd64.deb
sudo -u $(logname) yandex-disk setup

#install Viber
wget http://download.cdn.viber.com/cdn/desktop/Linux/viber.AppImage
sudo -u $(logname) mkdir /home/$(logname)/.apps
chmod ugo+x viber.AppImage
mv viber.AppImage /home/$(logname)/.apps
sudo -u $(logname) /home/$(logname)/.apps/viber.AppImage

#install telegram
snap install telegram-desktop

# cleanup
cd ../
rm -rf tmp